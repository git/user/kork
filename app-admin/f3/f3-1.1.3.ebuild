# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="Fight Flash Fraud, or Fight Fake Flash"
HOMEPAGE="http://oss.digirati.com.br/f3/"
SRC_URI="http://oss.digirati.com.br/${PN}/${PN}v${PV//./_}.zip"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_compile() {
	cc ${CFLAGS} ${LDFLAGS} -o f3write f3write.c || die
	cc ${CFLAGS} ${LDFLAGS} -o f3read f3read.c || die
}

src_install() {
	dobin f3write
	dobin f3read
}
