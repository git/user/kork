# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

inherit vim-plugin

DESCRIPTION="Vim-Improved eMACS: an Emacs emulation for Vim"
HOMEPAGE="http://www.algorithm.com.au/code/vimacs/index.html
	http://sigil.com.au/svn/trunk/Vimacs/
	http://www.vim.org/scripts/script.php?script_id=300"

SRC_URI="http://www.algorithm.com.au/downloads/vimacs/${P}.tar.gz"
LICENSE="Artistic-2"
SLOT="0"
KEYWORDS="~x86"
IUSE=""

VIM_PLUGIN_HELPFILES="vimacs tab-indent"


src_install () {
	dosym vim /usr/bin/vimacs || die
	dosym vim /usr/bin/vemacs || die
	dosym gvim /usr/bin/gvimacs || die
	dosym gvim /usr/bin/gvemacs || die

	vim-plugin_src_install
}
