# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit eutils

DESCRIPTION="General-Use FTools for manipulating and inspecting FITS files"
HOMEPAGE="http://heasarc.gsfc.nasa.gov/docs/software/ftools/ftools_menu.html"
SRC_URI="http://localhost/heasoft${PV}src.tar.gz"
LICENSE="UNKOWN freedist as-is GPL-2"
SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE=""
RESTRICT="test"

DEPEND="x11-base/xorg-server
		>=dev-lang/perl-5.6.0
		<dev-lang/perl-5.12.0"
RDEPEND="${DEPEND}"

S="${WORKDIR}/heasoft-${PV}/BUILD_DIR"
PREFIX="/usr/heasoft-${PVR}"

MSG="The HEASOFT General-Use FTools have been installed into
${PREFIX}.

To use the HEASOFT, you need to source the headas init script,
like so
	. \${HEADAS}/headas-init.sh
if you are using a bourne-shell. As a shortcut you can put an
alias in your ~/.bashrc:
	alias heainit=\". \${HEADAS}/headas-init.sh\"

C-shells should source \${HEADAS}/headas-init.csh instead.

The environment variable \${HEADAS} has been defined for you
globally in /etc/env.d/99headas."

pkg_setup() {
	[[ -d "${PREFIX}" ]] && die "Please unmerge previous installation."
	ewarn "This ebuild works but doesn't do things as god intended..."
	einfo "PREFIX=${PREFIX}"
}

src_prepare() {
	for subdir in tcl tk; do
		cd "${S}"/../tcltk/${subdir}/unix
		epatch "${FILESDIR}"/installManPage-S-namecollision.patch \
			|| die "epatch failed"
	done
	cd "${S}"/../ftools/guis/xdf
	sed -i '27iClientData Tk_MainWindow (Tcl_Interp *) ;' xdfShared.c || die
}

src_configure() {
	econf --prefix="${PREFIX}" || die "econf failed"
}

src_compile() {
	# -j1 and -j2 are ok, but -j3 is not.
	emake -j2 || die "emake failed"
}

src_install() {
	# The heasoft install scripts don't allow for $DESTDIR. Thus we
	# must let headas install itself. This is very, very bad.
	addwrite "${PREFIX}"
	einfo "Installing into ${PREFIX} directly..."
	# -j1 is needed, don't know why exactly
	emake -j1 install || die "emake install failed"
	einfo "Moving ${PREFIX} to image directory..."
	mkdir "${D}"/usr
	mv "${PREFIX}" "${D}"/usr

	echo "${MSG}" > WTF
	dodoc WTF

	HEADAS=`grep "HD_TOP_EXEC_PFX.*= " Makefile \
		| sed "s#^HD_TOP_EXEC_PFX.*= ##"`
	echo "HEADAS=\"${HEADAS}\"" > 99headas
	doenvd 99headas

	einfo "Heasoft's installation is a mess. A revdep-rebuild control "
	einfo "file is installed for ape and cfitsio, so that any dependencies "
	einfo "won't scream everytime you run revdep-rebuild."
	ape_so="`ls "${HEADAS}/lib/libape_*.so"`"
	ape_so="${ape_so##*/}"
	cfitsio_so="`ls "${HEADAS}/lib/libcfitsio_*.so"`"
	cfitsio_so="${ape_so##*/}"
	echo "# Heasoft's installation is a mess." > 70-heasoft
	echo "# This file makes it slightly less annoying for other" >> 70-heasoft
	echo "# packages using its libraries." >> 70-heasoft
	echo >> 70-heasoft
	echo "LD_LIBRARY_MASK=\"${ape_so} ${cfitsio_so}\"" >> 70-heasoft
	insinto /etc/revdep-rebuild/
	doins 70-heasoft
}

pkg_postinst() {
	elog "${MSG}"
}
