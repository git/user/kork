# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2
inherit git-r3 flag-o-matic

DESCRIPTION="A collection of FTools orchestrated by a pipeline driver"
HOMEPAGE="http://pulsar.sternwarte.uni-erlangen.de/fpipe/"
EGIT_REPO_URI="git://git.tokkee.org/fpipe.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~x86"
IUSE="debug +doc"

# sci-astronomy/ftools is only needed because of the ape library, cfitsio and
# xpa can be installed separately, an ebuild even exists for those.
DEPEND=">=x11-libs/gtk+-2.14
		media-gfx/gtkimageview
		dev-lang/perl
		dev-perl/gtk2-perl
		dev-perl/XML-Simple
		dev-perl/XML-SAX-Expat
		virtual/perl-Time-HiRes
		virtual/libusb:1
		doc? ( virtual/latex-base )
		>=sci-astronomy/ftools-6.10"

pkg_setup() {
	ewarn "Note: this software is not yet in a too mature status so expect some minor things to break"
	filter-ldflags -Wl,--as-needed
}

src_prepare() {
	epatch "${FILESDIR}"/pipeline_driver-Remove-references-to-IPC-XPA.patch
	cd software
	./bootstrap.sh || die "bootstrap failed"
}

src_configure() {
	# Get the new HEADAS location
	. "${ROOT}/etc/env.d/99headas"
	echo HEADAS="${HEADAS}"

	filter-flags -O2

	cd software
	econf $(use_enable debug debug 1) \
			--enable-fpixoacm2fits \
			--enable-fpixosvm2fits \
			--enable-fptemplate \
			|| die "econf failed"
}

src_compile() {
	cd software
	emake || die "emake failed"

	if use doc; then
		cd "${S}"/documents
		emake || die "emake failed for documents"
	fi
}

src_install() {
	cd software
	emake DESTDIR="${D}" install || die "emake install failed"

	cd "${S}"/example_files/pipelinedef
	insinto /usr/share/"${PN}"/pipelinedef
	doins *

	if use doc; then
		cd "${S}"/documents
		dodoc *.pdf *.odf
	fi
}

pkg_postinst() {
	elog "Before using this package you need to initialise heasoft by"
	elog "sourcing the headas-init script, like so"
	elog "	. ${HEADAS}/headas-init.sh"
	elog "heasoft is sci-astronomy/ftools"
	elog "Example pipelines have been installed into"
	elog "/usr/share/${PN}/pipelinedef"
}
