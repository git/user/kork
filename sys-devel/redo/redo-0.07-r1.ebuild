# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3
inherit eutils bash-completion-r1

DESCRIPTION="Smaller, easier, more powerful, and more reliable than make. An implementation of djb's redo."
HOMEPAGE="http://github.com/apenwarr/redo"
SRC_URI="https://github.com/apenwarr/${PN}/tarball/${P} -> ${P}.tar.gz"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

# We list the possible shells here, because redo creates a symlink to the one
# it favores.
DEPEND="|| (
			dev-lang/python:2.4
			dev-lang/python:2.5
			dev-lang/python:2.6
			dev-lang/python:2.7
		)
		dev-python/markdown
		|| (
			app-shells/dash
			app-shells/ksh
			app-shells/mksh
			app-shells/pdksh
			app-shells/bash
			app-shells/zsh
			sys-apps/busybox
		)"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	mv -v * "${S}" || die
}

# We use 'make' here, because then we can use 'emake'.
src_compile() {
	emake || die "emake failed"
}

src_install() {
	emake install DESTDIR="${D}" || die "emake install failed"

	dobashcomp contrib/bash_completion.d/redo

	dodoc "${D}/usr/share/doc/${PN}"/* && rm -r "${D}/usr/share/doc/${PN}/"

	dodoc README.md
}

src_test() {
	emake test || die "emake test failed"
}
