# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=6
inherit eutils bash-completion-r1
PYTHON_COMPAT=( python2_7 )
inherit python-single-r1

DESCRIPTION="Smaller, easier, more powerful, and more reliable than make. An implementation of djb's redo."
HOMEPAGE="http://github.com/apenwarr/redo"
SRC_URI="https://github.com/apenwarr/${PN}/tarball/${P} -> ${P}.tar.gz"

LICENSE="LGPL-2"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

# We list the possible shells here, because redo creates a symlink to the one
# it favores.
DEPEND="|| (
			dev-lang/python:2.4
			dev-lang/python:2.5
			dev-lang/python:2.6
			dev-lang/python:2.7
		)
		dev-python/markdown
		dev-python/beautifulsoup:python-2
		|| (
			app-shells/dash
			app-shells/ksh
			app-shells/mksh
			app-shells/pdksh
			app-shells/bash
			app-shells/zsh
			sys-apps/busybox
		)"
RDEPEND="${DEPEND}"

src_unpack() {
	unpack ${A}
	mv -v * "${S}" || die
}

# We use 'make' here, because then we can use 'emake'.
src_compile() {
	emake || die "emake failed"
}

src_install() {
	emake install DESTDIR="${D}" || die "emake install failed"

	python_doscript "${D}/usr/bin"/*

	dobashcomp contrib/bash_completion.d/redo

	dodoc "${D}/usr/share/doc/${PN}"/* && rm -r "${D}/usr/share/doc/${PN}/"

	dodoc README.md
}

src_test() {
	emake test || die "emake test failed"
}

pkg_postinst() {
	ewarn "redo changed the meaning of \$1, \$2, and \$3 to be compatible with"
	ewarn "djb redo. Please take that into account, or use the --old-args"
	ewarn "option until you do. Also, note that redo is still a very young"
	ewarn "project where such disruptive changes may still happen."
}
