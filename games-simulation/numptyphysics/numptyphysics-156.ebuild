EAPI="1"

inherit eutils subversion

DESCRIPTION="Numpty Physics is a drawing puzzle game in the spirit of Crayon Physics using the same excellent Box2D engine"
HOMEPAGE="http://numptyphysics.garage.maemo.org/"

# This is only the SRC_URI for the user levels. The code is in the SVN repo.
SRC_URI="user-levels? (
	http://numptyphysics.garage.maemo.org/levels/./butelo/./butelo.npz
	http://numptyphysics.garage.maemo.org/levels/./catalyst/./catalyst.npz
	http://numptyphysics.garage.maemo.org/levels/./christeck/./christeck.npz
	http://numptyphysics.garage.maemo.org/levels/./dneary/./dneary.npz
	http://numptyphysics.garage.maemo.org/levels/./gesualdi/./gesualdi.npz
	http://numptyphysics.garage.maemo.org/levels/./gnuton/./gnuton.npz
	http://numptyphysics.garage.maemo.org/levels/./gudger/./gudger.npz
	http://numptyphysics.garage.maemo.org/levels/./guile/./guile.npz
	http://numptyphysics.garage.maemo.org/levels/./hurd/./hurd.npz
	http://numptyphysics.garage.maemo.org/levels/./ioan/./ioan.npz
	http://numptyphysics.garage.maemo.org/levels/./jhoff80/./jhoff80.npz
	http://numptyphysics.garage.maemo.org/levels/./leonet/./leonet.npz
	http://numptyphysics.garage.maemo.org/levels/./melvin/./melvin.npz
	http://numptyphysics.garage.maemo.org/levels/./noodleman/./noodleman.npz
	http://numptyphysics.garage.maemo.org/levels/./papky/./papky.npz
	http://numptyphysics.garage.maemo.org/levels/./perli/./perli.npz
	http://numptyphysics.garage.maemo.org/levels/./qole/./qole.npz
	http://numptyphysics.garage.maemo.org/levels/./siminz/./siminz.npz
	http://numptyphysics.garage.maemo.org/levels/./szymanowski/./szymanowski.npz
	http://numptyphysics.garage.maemo.org/levels/./therealbubba/./therealbubba.npz
	http://numptyphysics.garage.maemo.org/levels/./werre/./werre.npz
	http://numptyphysics.garage.maemo.org/levels/./zeez/./zeez.npz
)"

ESVN_REPO_URI="https://vcs.maemo.org/svn/numptyphysics/trunk@${PV}"
LICENSE="GPL-2"

SLOT="0"
KEYWORDS="~x86 ~amd64"
IUSE="+user-levels"

RDEPEND="media-libs/sdl-ttf"
DEPEND="${RDEPEND}
	sys-devel/automake:1.8"

src_compile() {
	econf || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	emake DESTDIR="${D}" install || die "Install failed"

	insinto /usr/share/"${PN}"
	for f in ${A} ; do
		doins "${DISTDIR}"/${f}
	done
}
