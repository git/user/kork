#!/bin/sh

destdir="`dirname "$0"`"

destfile="$destdir"/user-levels.src_uri

tmpfile="$destdir"/index.html.XXXX

urlbase=http://numptyphysics.garage.maemo.org/levels
url=$urlbase/index.html

wget --quiet $url --output-document "$tmpfile"

grep 'href=.*\.npz' "$tmpfile" | sed -e 's#^.*href="##' -e 's#\.npz.*$#.npz#' \
	-e "s#^#$urlbase/#"

rm "$tmpfile"
