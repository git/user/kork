# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/games-simulation/simutrans/simutrans-0.102.2.2.ebuild,v 1.3 2010/05/24 18:57:09 pacho Exp $

EAPI=2
inherit flag-o-matic eutils games

MY_PV=${PV/0./}
MY_PV=${MY_PV//./-}
MY_FOOD_PV=${MY_PV/%-2/-1}
MY_128_SV=102.2.2
MY_128_PV=1.4.6--${MY_128_SV%.2}
MY_SRCFRG="http://sourceforge.net/projects/${PN}/files"

DESCRIPTION="A free Transport Tycoon clone"
HOMEPAGE="http://www.simutrans.com/"
# Note: The mirror:// syntax doesn't work here.
SRC_URI="
	${MY_SRCFRG}/simutrans/${MY_PV}/simutrans-src-${MY_PV}.zip/download -> simutrans-src-${MY_PV}.zip
	${MY_SRCFRG}/simutrans/${MY_PV}/simulinux-${MY_PV}.zip/download -> simulinux-${MY_PV}.zip
	${MY_SRCFRG}/pak64/${MY_PV}/simupak64-${MY_PV}.zip/download -> simupak64-${MY_PV}.zip
	${MY_SRCFRG}/pak64/${MY_PV}/simupak64-addon-food-${MY_PV}.zip/download -> simupak64-addon-food-${MY_FOOD_PV}.zip
	pak128? ( ${MY_SRCFRG}/pak128/pak128%20for%20${MY_128_SV//./-}/pak128-${MY_128_PV}.zip/download -> simupak128-${MY_128_PV}.zip )
	"

LICENSE="Artistic"
SLOT="0"
KEYWORDS="-* amd64 x86"
IUSE="+pak128"

RDEPEND="media-libs/libsdl[audio,video]
	sys-libs/zlib
	media-libs/libpng
	media-libs/sdl-mixer"
DEPEND="${RDEPEND}
	app-arch/unzip"

S=${WORKDIR}

src_prepare() {
	strip-flags # bug #293927
	echo "BACKEND=mixer_sdl
COLOUR_DEPTH=16
OSTYPE=linux
FLAGS=-DSTEPS16" > config.default \
	|| die "echo failed"

	if use amd64; then
		echo "FLAGS+=-DUSE_C" >> config.default
	fi
	# make it look in the install location for the data
	sed -i \
		-e "s:argv\[0\]:\"${GAMES_DATADIR}/${PN}/\":" \
		simmain.cc \
		|| die "sed failed"

	# Please don't override our CFLAGS, kthx
	sed -i \
		-e '/-O$/d' \
		Makefile \
		|| die "sed failed"

	rm -f simutrans/simutrans
}

src_install() {
	newgamesbin sim ${PN} || die "dogamesbin failed"
	insinto "${GAMES_DATADIR}"/${PN}
	doins -r simutrans/* || die "doins failed"
	dodoc documentation/* todo.txt
	doicon simutrans.ico
	make_desktop_entry simutrans Simutrans /usr/share/pixmaps/simutrans.ico
	prepgamesdirs
}
